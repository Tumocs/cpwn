CC = gcc
CFLAGS = -g -Wall -Wextra -Wpedantic
LDLIBS = -lcrypto -lcurl
OBJECTS = main.o hash.o curl_related.o

cpwn: main.o hash.o curl_related.o
	$(CC) $(CFLAGS) $(OBJECTS) -o cpwn $(LDLIBS)

main.o: main.c

hash.o: hash.c

curl_related.o: curl_related.c

clean:
	rm -fv main.o hash.o curl_related.o cpwn
