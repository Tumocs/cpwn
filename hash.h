#ifndef _GETSHA_H
#define _GETSHA_H

//simple line reading function
char * fetchpass (void);

//get SHA1 from string
unsigned char* getsha (char * string);

//compare local SHA to list of SHA:s received from the server
int compare (char * hash, char * hashhead, char * bulk);

#endif //_GETSHA_H
