#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "curl_related.h"

size_t WriteMemoryCallback(void *contents, size_t size, 
        size_t nmemb, void *userp) {
    size_t realsize = size * nmemb;
    Storage *mem = (Storage*)userp;

    char *ptr = realloc(mem->memory, mem->size + realsize +1);
    if(ptr == NULL) {
        printf("out of memory");
        return 0;
    }

    mem->memory = ptr;
    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

int curl_error_report (CURLcode error) {
    if (error) {
        const char * errstr = curl_easy_strerror(error);
        puts (errstr);
        return 1;
    }
    return 0;
}
