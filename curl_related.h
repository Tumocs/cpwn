#ifndef _CURL_RELATED_H
#define _CURL_RELATED_H

#include <curl/curl.h>

typedef struct Storage {
    char * memory;
    size_t size;
} Storage;

size_t WriteMemoryCallback(void *contents, size_t size, 
        size_t nmemb, void *userp);


int curl_error_report (CURLcode error);



#endif // _CURL_RELATED_H
