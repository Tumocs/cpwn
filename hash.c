#include <openssl/sha.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "hash.h"

char * fetchpass (void) {
    char * str = malloc(sizeof(char) * 50);
    char * p = str;
    int maxlen = 50;
    int len = maxlen;
    char c;

    while (c != EOF) {
        c = fgetc(stdin);

        if (c == '\n') {
            c = '\0';
        }

        if (--len == 0) {
            len = maxlen;
            char * temp = realloc(str, maxlen *=2);
            
            if (temp == NULL) {
                free(str);
                return NULL;
            }

            p = temp + (p - temp);
            str = temp;
        }

        if ((*p++ = c) == '\0') {
            break;
        }

    }

    *p = '\0';

    return str;
}

unsigned char* getsha(char * string) {
    unsigned char * hash = malloc(sizeof(unsigned char) * SHA_DIGEST_LENGTH);
    size_t length = strlen(string);

    SHA1((unsigned char *)string, length, hash);

    return hash;

}

int compare (char * hash, char * hashhead, char * bulk) {
    char tempstring[41] = {'\0'};
    int strcounter = 0;
    for (char * i = bulk; *i; i++) {
        if (tempstring[0] == '\0') {
            strcpy(tempstring, hashhead);
            strcounter = 5;
        }
        if (strcounter < 40 && *i < 123 && *i > 47) {
            char tmp = *i;
            if (tmp <= 'Z' && tmp >= 'A') {
                tmp += 32;
            }
            tempstring[strcounter] = tmp;
            tempstring[++strcounter] = '\0';
        }

        if (strcounter == 40) {
            if(!strcmp(hash, tempstring)) {
                return 1;
            }
            strcounter++;
        }

        if (*i == '\n') {
            tempstring[0] = '\0';
            strcounter = 0;
        }

    } 

    return 0;

}
