#include <stdio.h>
#include <stdlib.h>
#include <openssl/sha.h>
#include <curl/curl.h>
#include <string.h>
#include "hash.h"
#include "curl_related.h"

int main (void) {

    //used for curl write callback
    Storage storage = { 0,0 };
    
    static char * baseurl = "https://api.pwnedpasswords.com/range/";
    char * str;
    unsigned char * hash;
    char hashhead[6];
    char readable[SHA_DIGEST_LENGTH * 2];
    char * curlstring;
    CURL * curl;

    if((curl_error_report(curl_global_init(CURL_GLOBAL_DEFAULT)))) {
        return 0;
    }

    curl = curl_easy_init();

    puts("Enter password to check:");
    str = fetchpass();
    hash = getsha(str);

    //set hash to a bit more readable state
    for (int i=0; i<SHA_DIGEST_LENGTH; i++) {
        sprintf((char*)&(readable[i*2]), "%02x", hash[i]);
    }

    //extract first 5 characters of has to send to API
    for (int i=0; i<5; i++) {
        hashhead[i] = readable[i];
    }
    hashhead[5] = '\0';
    
    //construct the API call
    curlstring = malloc(sizeof(char) * (strlen(baseurl) + 6));
    sprintf(curlstring, "%s%s", baseurl, hashhead);

    if(curl) {
        //set up CURL and check for errors on the way
        if (curl_error_report(curl_easy_setopt(curl, CURLOPT_URL, curlstring)) ||
            curl_error_report(curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, 
                                               WriteMemoryCallback)) ||
            curl_error_report(curl_easy_setopt(curl, CURLOPT_WRITEDATA, 
                                               (void *)&storage)) ||
            curl_error_report(curl_easy_perform(curl))) {

            //clean everything if any errors are detected
            curl_easy_cleanup(curl);
            curl_global_cleanup();
            free(curlstring);
            free(hash);
            free(storage.memory);
            memset(str, 0, strlen(str));
            free(str);
            return 0;

        }

        curl_easy_cleanup(curl);
    }
    
    if(compare(readable, hashhead, storage.memory)) {
        printf("Password is compromized\n");
    }

    else {
        printf("This password should be safe to use\n");
    }

    //clean
    curl_global_cleanup();
    free(curlstring);
    free(hash);
    memset(str, 0, strlen(str));
    free(str);
    free(storage.memory);

    return 0;

}
